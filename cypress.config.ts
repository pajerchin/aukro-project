import { defineConfig } from "cypress";
const allureWriter = require("@shelex/cypress-allure-plugin/writer");

export default defineConfig({
  retries: {
    runMode: 1,
  },
  defaultCommandTimeout: 60000,
  requestTimeout: 60000,
  reporter: "junit",
  chromeWebSecurity: false,
  reporterOptions: {
    mochaFile: "junit/results-[hash].xml",
  },
  e2e: {
    baseUrl: "https://aukro.cz",
    setupNodeEvents(on, config) {
      allureWriter(on, config);
      return config;
    },
  },
});
