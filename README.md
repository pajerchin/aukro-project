#### Cypress screenshots pri neuspesnom teste
- logi z testov su zaznamenane v screenshote ked test spadne
- kod ktory to zabezpecuje je v sekcii cypress/utils/screenlogger.utils.ts

![failed test](./cypress/screenshots/order.cy.ts/failed.png)

#### Cypress video z test runu

[![video](./cypress/videos/order.gif)](./cypress/videos/order.gif)

#### Allure report

- deploy v pipeline na gitlab pages

[Allure report ](https://pajerchin.gitlab.io/aukro-project/)
