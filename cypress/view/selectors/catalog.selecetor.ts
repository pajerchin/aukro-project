export enum CATALOG_SELECTORS {
  catalogItem = "auk-list-view > auk-list-card",
  guaranteeBadge = "#money-back-guarantee2:visible",
  linkItem = "a[id^=offer]",
}
