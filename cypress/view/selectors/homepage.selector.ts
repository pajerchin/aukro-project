export enum HOMEPAGE_SELECTORS {
  guaranteeCheckbox = "#mat-checkbox-10-input",
  chooseCategoryButton = ".display-inline-block-min-tablet",
  headerNavbar = "auk-header-navbar",
  topCategory = "auk-top-level-category",
  categoryListItems = "auk-list-card",
}
