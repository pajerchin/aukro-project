export enum PRODUCT_DETAIL_SELECTORS {
  guaranteeBadge = "#money-back-guarantee",
  priceItem = ".tw-text-5xl",
  shopValueItemInput = "input[aukonlynumberinput]",
  shoppingCart = "auk-basket-control > span",
  auctionItemDetailInput = "auk-item-detail-number-input",
  auctionHint = "auk-hint",
}
