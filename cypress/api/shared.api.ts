export function loadAPI() {
  cy.intercept("GET", "/backend-web/api/token/isPairedToSubscribedUser", {
    statusCode: 500,
  }).as("popup");
}
