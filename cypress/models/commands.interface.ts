declare namespace Cypress {
  interface Chainable {
    openPage: (view: Cypress.ViewportPreset, url: string) => Cypress.Chainable;
    agreeWithCookies: () => Cypress.Chainable;
    openHeaderNavbar: () => Cypress.Chainable;
    checkboxChecked: (selector: string) => Cypress.Chainable;
    checkLocationParam: (
      param: keyof Location,
      locationValue: string
    ) => Cypress.Chainable;
    checkListGuaranteeBadges: () => Cypress.Chainable;
    openMiddleItem: () => Cypress.Chainable;
    checkGuaranteeBadgeInProductDetail: () => Cypress.Chainable;
    clickOnButtonByText: (buttonText: string) => Cypress.Chainable;
    increasePriceInAuction: (increase: number) => Cypress.Chainable;
    shoppingCart: () => Cypress.Chainable;
    checkLengthListitems: (countItems: number) => Cypress.Chainable;
    chooseCategory: (countItems: number) => Cypress.Chainable;
    shopAction: () => Cypress.Chainable;
    enterValueToInput: (selector: string, value: string) => Cypress.Chainable;
  }
}
