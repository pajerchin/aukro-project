import { SHARED_SELECTORS } from "../view/selectors/shared.selectors";

Cypress.Commands.add(
  "openPage",
  (view: Cypress.ViewportPreset, url: string) => {
    cy.viewport(view).log(`open viewport: ${view}`);
    cy.visit(url).log(`url ${url} and ${view} is open`);
  }
);

Cypress.Commands.add("agreeWithCookies", () => {
  cy.get(SHARED_SELECTORS.agreeWithCookies)
    .click()
    .should("not.exist")
    .log("close cookie popup");
});

Cypress.Commands.add("checkboxChecked", (selector: string) => {
  cy.get(selector)
    .check({ force: true })
    .should("be.checked")
    .log("button is checked");
});

Cypress.Commands.add(
  "checkLocationParam",
  (param: keyof Location, locationValue: string) => {
    cy.location(param)
      .should("contain", locationValue)
      .log(`Url param ${param} contain ${locationValue}`);
  }
);

Cypress.Commands.add("enterValueToInput", (selector: string, value: string) => {
  cy.get(selector)
    .clear()
    .should("be.empty")
    .focus()
    .type(value)
    .blur()
    .should("have.value", value)
    .log(`input ${selector} have value ${value}`);
});
