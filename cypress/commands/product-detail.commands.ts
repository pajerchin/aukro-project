import { LOGIN_SELECTORS } from "../view/selectors/login.selectors";
import { PRODUCT_DETAIL_SELECTORS } from "../view/selectors/product-detail.selector";

Cypress.Commands.add("checkGuaranteeBadgeInProductDetail", () => {
  cy.get(PRODUCT_DETAIL_SELECTORS.guaranteeBadge)
    .should("exist")
    .and("be.visible")
    .log("check guarantee in product detail");
});

Cypress.Commands.add("clickOnButtonByText", (buttonText: string) => {
  cy.contains(buttonText)
    .should("be.visible")
    .click()
    .log(`Click on button ${buttonText}`);
});

Cypress.Commands.add("increasePriceInAuction", (increase: number) => {
  cy.get(PRODUCT_DETAIL_SELECTORS.priceItem)
    .invoke("text")
    .then((price) => {
      const increasePrice = Math.floor(
        Number(price.replace(/[^\d]/g, "")) * increase
      );
      cy.enterValueToInput(
        PRODUCT_DETAIL_SELECTORS.shopValueItemInput,
        String(increasePrice)
      );
    })
    .log(`increase price about ${increase}`);
});

Cypress.Commands.add("shoppingCart", () => {
  cy.get(PRODUCT_DETAIL_SELECTORS.shoppingCart)
    .invoke("attr", "data-count")
    .should("exist")
    .and("not.be.empty")
    .log("shopping cart is not empty");
});

Cypress.Commands.add("shopAction", () => {
  cy.get(PRODUCT_DETAIL_SELECTORS.auctionItemDetailInput).then(
    ($itemDetail) => {
      if ($itemDetail.find(PRODUCT_DETAIL_SELECTORS.auctionHint).length > 0) {
        cy.increasePriceInAuction(1.2);
        cy.clickOnButtonByText("Přihodit");
        cy.get(LOGIN_SELECTORS.loginForm)
          .should("exist")
          .and("be.visible")
          .log("login form is visible");
      } else {
        cy.clickOnButtonByText("Koupit");
        cy.shoppingCart();
      }
    }
  );
});
