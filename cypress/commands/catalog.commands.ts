import { CATALOG_SELECTORS } from "../view/selectors/catalog.selecetor";

Cypress.Commands.add("checkListGuaranteeBadges", () => {
  cy.get(CATALOG_SELECTORS.catalogItem)
    .each(($item) => {
      cy.wrap($item).find(CATALOG_SELECTORS.guaranteeBadge).should("exist");
    })
    .log("check guarantee badges in catalog list");
});

Cypress.Commands.add("openMiddleItem", () => {
  cy.get(CATALOG_SELECTORS.catalogItem).then(($items) => {
    const middleItem = Math.ceil($items.length / 2);
    cy.wrap($items)
      .eq(middleItem)
      .find(CATALOG_SELECTORS.linkItem)
      .first()
      .click({ force: true })
      .should("not.exist")
      .log("open middle item");
  });
});
