import { HOMEPAGE_SELECTORS } from "../view/selectors/homepage.selector";

Cypress.Commands.add("openHeaderNavbar", () => {
  cy.get(HOMEPAGE_SELECTORS.chooseCategoryButton)
    .click()
    .find(HOMEPAGE_SELECTORS.headerNavbar)
    .should("have.class", "active")
    .log("open header navbar");
});

Cypress.Commands.add("chooseCategory", (countItems: number) => {
  let shouldStop = false;

  cy.get(HOMEPAGE_SELECTORS.topCategory)
    .find("a")
    .each(($categoryLink) =>
    // ked narazi na kategoriu ktora vyhovuje poziadavke tak sa cyklus zastavi
      cy.then(() => {
        if (shouldStop) {
          return false;
        }

        cy.openHeaderNavbar();

        cy.wrap($categoryLink)
          .should("be.visible")
          .click()
          .invoke("attr", "href")
          .then((href) => {
            cy.checkLocationParam("pathname", href);
          });

        // cy.checkboxChecked(HOMEPAGE_SELECTORS.guaranteeCheckbox) pouzil by som tento selector ale neviem ci sa id menia u vas;
        cy.clickOnButtonByText("Garance vrácení peněz");

        cy.checkLocationParam("search", "paymentViaAukro=true");

        cy.get(HOMEPAGE_SELECTORS.categoryListItems)
          .its("length")
          .should("be.greaterThan", countItems)
          .then((items) => {
            if (items > countItems) {
              shouldStop = true;
            }
          })
          .log("choose category");
      })
    );
});
