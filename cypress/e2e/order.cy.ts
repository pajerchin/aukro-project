import { loadAPI } from "../api/shared.api";
import { URL } from "../view/pages/url.page";
describe("Case studie Aukro", () => {
  beforeEach(() => {
    loadAPI();
  });

  it("Test", () => {
    cy.openPage("macbook-15", URL.homepage);

    cy.agreeWithCookies();

    cy.chooseCategory(4);

    cy.checkListGuaranteeBadges();

    cy.openMiddleItem();

    cy.checkGuaranteeBadgeInProductDetail();

    cy.shopAction();
  });
});
