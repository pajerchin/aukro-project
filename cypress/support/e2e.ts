// Import commands.js using ES2015 syntax:
import "../commands/homepage.commands";
import "../commands/shared.commands";
import "../commands/catalog.commands";
import "../commands/product-detail.commands";

import "../utils/screenlogger.utils.ts";
import "@shelex/cypress-allure-plugin";

Cypress.on("uncaught:exception", () => {
  return false;
});
